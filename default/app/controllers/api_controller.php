<?php
class ApiController extends RestController{

	public function get_estados(){
		$result = array(
			array(
				'id' => '1',
				'nombre' => 'Pendiente'
			),
			array(
				'id' => '2',
				'nombre' => 'En proceso'
			),
			array(
				'id' => '3',
				'nombre' => 'Finalizada'
			),
			array(
				'id' => '4',
				'nombre' => 'Cancelada'
			)
		);
        $this->data = $result;
    }

    public function get_tareas(){
        $this->data = (new Tareas)->find();
    }

    public function post_tareas(){
    	$tarea = new Tareas();
    	$tarea->titulo = $this->param()['titulo'];
    	$tarea->descripcion = $this->param()['descripcion'];
    	$tarea->save();
    	$this->data = $tarea;
    }

    public function put_tareas($idTarea = 0){
    	$tarea = (new Tareas)->find_first($idTarea);
    	if(is_object($tarea)){
    		$tarea->titulo = $this->param()['titulo'];
    		$tarea->descripcion = $this->param()['descripcion'];
    		$tarea->estado_id = $this->param()['estado_id'];
    		$tarea->save();
    	}
    	$this->data = $tarea;
    }

    public function delete_tareas($idTarea = 0){
    	$this->data = (new Tareas)->delete($idTarea);
    }

}