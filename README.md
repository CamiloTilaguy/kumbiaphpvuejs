![KumbiaPHP logo](http://recaudoefectivo.co/dev/master/img/adjuntos/f-187-5b4fd052777b9.png)

---
## Ejemplo de como usar VueJS y kumbiaphp

Aqui se encuentra el ejemplo de un CRUD con vueJS y el API Rest que facilita kumbiaphp

Espero poder hacer el post en el blog de kumbiaphp muy pronto

Lo importante de este ejemplo se encuentra en 4 archivos:

* En el template cargar vueJS https://bitbucket.org/CamiloTilaguy/kumbiaphpvuejs/src/master/default/app/views/_shared/templates/default.phtml

* En la vista se encuentra el template vuejs y los v-bind https://bitbucket.org/CamiloTilaguy/kumbiaphpvuejs/src/master/default/app/views/index/index.phtml

* El api son pocas lineas gracias a las facilidades de Kumbiaphp https://bitbucket.org/CamiloTilaguy/kumbiaphpvuejs/src/master/default/app/controllers/api_controller.php

* La logica vueJS va en el tipico archivo app.js https://bitbucket.org/CamiloTilaguy/kumbiaphpvuejs/src/master/default/public/javascript/app.js



## Slack KumbiaPHP Channel in spanish and english
http://slack.kumbiaphp.com (new)

Manual
===
* importar base de datos que esta en /default/app/config/tareasvue.sql
* actualizar el archivo /default/app/config/databases.ini con los datos de acceso a la BD
* Listo

Licencia
===
New BSD
